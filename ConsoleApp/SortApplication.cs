﻿using System;
using Sorting;

namespace ConsoleApp
{
    internal static class SortApplication
    {
        public static void Main()
        {
            Console.Write("Введите путь к входному файлу: ");
            var inputFilePath = Console.ReadLine();
            Console.Write("Введите путь к выходному файлу: ");
            var outputFilePath = Console.ReadLine();

            new ExternalFileSorter().Sort(inputFilePath, outputFilePath);
        }
    }
}