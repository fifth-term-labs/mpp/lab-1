﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Sorting;

namespace WinFormsApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void sortFileButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            var inputFilePath = openFileDialog.FileName;

            if (saveFileDialog.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            var outputFilePath = saveFileDialog.FileName;
            new ExternalFileSorter().Sort(inputFilePath, outputFilePath);
            MessageBox.Show("Файл отсортирован!");
        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}