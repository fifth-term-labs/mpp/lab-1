﻿namespace Sorting.Heap
{
    public class MinHeapNode
    {
        public string Element { get; set; }
        public int BlockIndex { get; }

        public MinHeapNode(string element, int blockIndex)
        {
            Element = element;
            BlockIndex = blockIndex;
        }
    }
}