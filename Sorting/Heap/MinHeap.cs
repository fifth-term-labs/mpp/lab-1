﻿using System;
using System.Linq;

namespace Sorting.Heap
{
    public class MinHeap
    {
        private MinHeapNode[] _heapNodes;

        public MinHeap(MinHeapNode[] heapNodes)
        {
            _heapNodes = heapNodes;
            var rootIndex = (_heapNodes.Length - 1) / 2;

            while (rootIndex >= 0)
            {
                MinHeapify(rootIndex);
                rootIndex--;
            }
        }

        private void MinHeapify(int rootIndex)
        {
            var leftIndex = GetLeftIndex(rootIndex);
            var rightIndex = GetRightIndex(rootIndex);

            var smallestIndex = rootIndex;

            if (leftIndex < _heapNodes.Length && IsFirstElementLessThanSecondElement(leftIndex, rootIndex))
            {
                smallestIndex = leftIndex;
            }

            if (rightIndex < _heapNodes.Length && IsFirstElementLessThanSecondElement(rightIndex, smallestIndex))
            {
                smallestIndex = rightIndex;
            }

            if (smallestIndex == rootIndex)
            {
                return;
            }

            SwapNodes(rootIndex, smallestIndex);
            MinHeapify(smallestIndex);
        }

        private static int GetLeftIndex(int rootIndex)
        {
            return 2 * rootIndex + 1;
        }

        private static int GetRightIndex(int rootIndex)
        {
            return 2 * rootIndex + 2;
        }

        private bool IsFirstElementLessThanSecondElement(int firstElementIndex, int secondIndex)
        {
            return string.Compare(
                _heapNodes[firstElementIndex].Element,
                _heapNodes[secondIndex].Element, StringComparison.Ordinal) < 0;
        }

        private void SwapNodes(int i, int j)
        {
            var temporaryNode = _heapNodes[i];
            _heapNodes[i] = _heapNodes[j];
            _heapNodes[j] = temporaryNode;
        }

        public MinHeapNode GetMinElement()
        {
            return _heapNodes.Length > 0 ? _heapNodes[0] : null;
        }

        public void ReplaceMinElement(MinHeapNode newNode)
        {
            _heapNodes[0] = newNode;

            if (_heapNodes[0].Element == null)
            {
                _heapNodes = _heapNodes.Skip(1).ToArray();
            }

            MinHeapify(0);
        }
    }
}