﻿using System;
using System.Collections.Generic;
using System.IO;
using Sorting.Heap;

namespace Sorting
{
    public class ExternalFileSorter
    {
        private const string TempDirectoryName = ".SortFiles";
        private const int NumberOfLinesInBlock = 800000;
        private const int StreamBufferSize = 50 * 1024 * 1024; // 50 MB

        private readonly Dictionary<int, StreamReader> _streamReadersDictionary =
            new Dictionary<int, StreamReader>();

        public void Sort(string inputFilePath, string outputFilePath)
        {
            CreateTemporaryDirectory();
            var numberOfSortedBlocks = DivideInputFileToSortedFileBlocks(inputFilePath);
            MergeSortedBlocks(numberOfSortedBlocks, outputFilePath);
            CloseStreamReaders();
            RemoveTemporaryDirectory();
        }

        private static void CreateTemporaryDirectory()
        {
            RemoveTemporaryDirectory();
            Directory.CreateDirectory(TempDirectoryName);
        }

        private void CloseStreamReaders()
        {
            foreach (var streamReaderPair in _streamReadersDictionary)
            {
                streamReaderPair.Value.Close();
            }
        }

        private static void RemoveTemporaryDirectory()
        {
            if (Directory.Exists(TempDirectoryName))
            {
                Directory.Delete(TempDirectoryName, true);
            }
        }

        private static int DivideInputFileToSortedFileBlocks(string inputFilePath)
        {
            var inputFileReader = new StreamReader(
                inputFilePath,
                System.Text.Encoding.Unicode,
                true,
                StreamBufferSize
            );

            var numberOfSortedBlocks = 0;

            while (!inputFileReader.EndOfStream)
            {
                var lines = ReadBlock(inputFileReader);
                lines.Sort();

                SaveSortedBlock(numberOfSortedBlocks, lines);
                numberOfSortedBlocks++;
            }

            inputFileReader.Close();
            return numberOfSortedBlocks;
        }

        private static List<string> ReadBlock(TextReader inputFileReader)
        {
            Console.WriteLine($"ReadBlock() start");

            string line;
            var lines = new List<string>(NumberOfLinesInBlock);

            while (lines.Count < NumberOfLinesInBlock && (line = inputFileReader.ReadLine()) != null)
            {
                lines.Add(line);
            }

            Console.WriteLine($"ReadBlock() finish");
            return lines;
        }

        private static void SaveSortedBlock(int sortedBlockIndex, IList<string> lines)
        {
            Console.WriteLine($"SaveSortedBlock() {sortedBlockIndex} start");

            var fileWriter = new StreamWriter(
                $"{TempDirectoryName}/{sortedBlockIndex}.txt",
                false,
                System.Text.Encoding.Unicode,
                StreamBufferSize
            ) {AutoFlush = false};

            if (lines.Count != 0)
            {
                fileWriter.Write(lines[0]);
            }

            for (var i = 1; i < lines.Count; i++)
            {
                fileWriter.WriteLine();
                fileWriter.Write(lines[i]);
            }

            fileWriter.Close();
            Console.WriteLine($"SaveSortedBlock() {sortedBlockIndex} finish");
        }

        private void MergeSortedBlocks(int numberOfSortedBlocks, string outputFilePath)
        {
            Console.WriteLine("MergeSortedBlocks() start");
            var heapNodes = new MinHeapNode[numberOfSortedBlocks];

            for (var i = 0; i < numberOfSortedBlocks; i++)
            {
                var line = ReadLine(i);
                if (line == null)
                {
                    break;
                }

                heapNodes[i] = new MinHeapNode(line, i);
            }

            var heap = new MinHeap(heapNodes);
            var amountOfFiles = 0;

            var fileWriter = new StreamWriter(
                outputFilePath,
                false,
                System.Text.Encoding.Unicode,
                StreamBufferSize
            ) {AutoFlush = false};

            while (amountOfFiles != numberOfSortedBlocks)
            {
                var rootNode = heap.GetMinElement();

                if (rootNode == null)
                {
                    continue;
                }

                fileWriter.WriteLine(rootNode.Element);
                rootNode.Element = ReadLine(rootNode.BlockIndex);

                if (rootNode.Element == null)
                {
                    amountOfFiles++;
                }

                heap.ReplaceMinElement(rootNode);
            }

            fileWriter.Close();
            Console.WriteLine("MergeSortedBlocks() finish");
        }

        private string ReadLine(int sortedBlockIndex)
        {
            if (!_streamReadersDictionary.ContainsKey(sortedBlockIndex))
            {
                _streamReadersDictionary[sortedBlockIndex] =
                    new StreamReader(
                        $"{TempDirectoryName}/{sortedBlockIndex}.txt",
                        System.Text.Encoding.Unicode,
                        true,
                        StreamBufferSize
                    );
            }

            return _streamReadersDictionary[sortedBlockIndex].ReadLine();
        }
    }
}