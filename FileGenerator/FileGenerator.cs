﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;

namespace FileGenerator
{
    internal static class FileGenerator
    {
        private static readonly char[] Symbols =
        {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        };

        private const int MinWordsCount = 3;
        private const int MaxWordsCount = 10;
        private const int MinLettersCount = 2;
        private const int MaxLettersCount = 20;
        private const int SymbolSizeInBytes = sizeof(char);

        public static void Main()
        {
            Console.Write("Введите путь к генерируемому файлу: ");
            var filePath = Console.ReadLine();

            if (filePath == null)
            {
                return;
            }

            Console.Write("Введите размер файла (в байтах): ");
            var fileSizeInBytes = Convert.ToInt64(Console.ReadLine());

            var writer = new StreamWriter(
                filePath,
                false,
                System.Text.Encoding.Unicode,
                50 * 1024 * 1024 // 50 MB
            ) {AutoFlush = false};

            var random = new Random();

            while (fileSizeInBytes > 0)
            {
                var wordsCount = random.Next(MinWordsCount, MaxWordsCount + 1);

                for (var i = 0; i < wordsCount && fileSizeInBytes > 0; i++)
                {
                    var lettersCount = random.Next(MinLettersCount, MaxLettersCount + 1);

                    for (var j = 0; j < lettersCount && fileSizeInBytes > 0; j++)
                    {
                        var randomIndex = random.Next(0, Symbols.Length);
                        writer.Write(Symbols[randomIndex]);
                        fileSizeInBytes -= SymbolSizeInBytes;
                    }
                }

                if (fileSizeInBytes <= 0)
                {
                    continue;
                }

                writer.WriteLine();
                fileSizeInBytes -= Environment.NewLine.Length * SymbolSizeInBytes;
            }

            writer.Close();
        }
    }
}